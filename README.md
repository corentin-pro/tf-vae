## Soremuscle Tf VAE

This project is a tensorflow implementation of the original VAE paper (https://arxiv.org/abs/1312.6114).


## Dependencies

To run the training you need `Python3` with the python package `tensorflow`/`tensorflow-gpu` and `numpy` (will be installed automatically with tensorflow).

You can install the python packages using `pip` (`sudo apt install python3-pip` on debian based linux), with the command :

```
pip install tensorflow 
```

(`tensorflow-gpu` is recommended if you have a CUDA capable GPU, for more information see https://www.tensorflow.org/install/)



## Usage

Training teh model :

```
python train.py --checkpoint path/to/desired/folder
```

Note : the checkpoint path will be created if needed. If a folder already exists it will be replaced.

