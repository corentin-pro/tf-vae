# coding: utf-8

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


class ModelConfig(type):
    KERNEL_SIZE = 3

    CONV_FEATURES = [8, 16, 32]

    LATENT_DIMENSIONS = 2
