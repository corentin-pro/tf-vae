# coding: utf-8

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import json
import os
import resource
import shutil
import subprocess
import time

import numpy as np

from model import inference, ModelConfig
from train_config import TrainConfig


def save_mnist_data(save_path):
    """Retrieve mnist data and save the numpy array at the given path"""
    from tensorflow.examples.tutorials.mnist import input_data

    mnist_folder = "MNIST_data"
    mnist_data = input_data.read_data_sets(mnist_folder, one_hot=True)
    shutil.rmtree(mnist_folder)
    np.save(save_path, mnist_data.train._images)


def train():
    """..."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--checkpoint', help='Folder to save checkpoint and event')
    args = parser.parse_args()

    import tensorflow as tf

    if not os.path.exists(TrainConfig.TRAIN_DATA_FILE):
        save_mnist_data(TrainConfig.TRAIN_DATA_FILE)
    train_images = np.load(TrainConfig.TRAIN_DATA_FILE)

    # manage memory(True->if we need then use, False->use all.This is same as default)
    gpu_options = tf.GPUOptions(allow_growth=True)
    session_config = tf.ConfigProto(gpu_options=gpu_options)

    with tf.Session(config=session_config).as_default() as sess:
        # Setting the variables
        symbolic_input = tf.placeholder(
            tf.float32,
            shape=[None, TrainConfig.OUTPUT_SHAPE[0] * TrainConfig.OUTPUT_SHAPE[1] * TrainConfig.OUTPUT_SHAPE[2]],
            name="symbolic_input")
        test_latent = tf.placeholder(
            tf.float32,
            shape=[None, ModelConfig.LATENT_DIMENSIONS],
            name="test_latent")
        is_training = tf.placeholder(tf.bool, shape=[], name="is_training")
        batch_norm_training = tf.placeholder(tf.bool, shape=[], name="batch_norm_training")
        test_image = tf.placeholder(
            tf.float32,
            shape=[1,
                   TrainConfig.OUTPUT_SHAPE[0] * TrainConfig.TEST_IMAGE_COLUMNS,
                   TrainConfig.OUTPUT_SHAPE[1] * TrainConfig.TEST_IMAGE_ROWS,
                   TrainConfig.OUTPUT_SHAPE[2]],
            name="symbolic_input")

        reshape_input = tf.reshape(
                symbolic_input,
                [-1, TrainConfig.OUTPUT_SHAPE[0], TrainConfig.OUTPUT_SHAPE[1], TrainConfig.OUTPUT_SHAPE[2]])

        train_summaries = []
        image_summaries = []
        test_summaries = []

        # with tf.variable_scope("Normalization"):
        #     normalized_input = (reshape_input * 2) - 1
        normalized_input = reshape_input

        with tf.variable_scope("Network"):
            network_output = inference(normalized_input, test_latent, is_training, batch_norm_training)
            decoder_op = network_output["decoder_output"]
            sampler_mean_op = network_output["sampler_mean"]
            sampler_logdev_op = network_output["sampler_logdev"]

        image_summaries.append(tf.summary.image(
            "decoder_output", decoder_op, max_outputs=TrainConfig.IMAGE_MAX_OUTPUT))
        test_summaries.append(tf.summary.image("test_image", test_image, max_outputs=1))
        # image_summaries.append(tf.summary.image(
        #     "normalized_input", normalized_input, max_outputs=TrainConfig.IMAGE_MAX_OUTPUT))
        # train_summaries.append(tf.summary.histogram("normalized_input", normalized_input))
        # train_summaries.append(tf.summary.histogram("decoder_output", decoder_op))

        # -------- Train and Evaluate the Mode --------

        print("\nInitializing trainer...", end="\r")
        with tf.variable_scope("Train"):
            step_op = tf.Variable(0, trainable=False, name="Step")

            with tf.variable_scope("loss"):
                with tf.variable_scope("decoder"):
                    decoder_loss_op = -tf.reduce_sum(
                        normalized_input * tf.log(1e-8 + decoder_op)
                        + (1 - normalized_input) * tf.log(1e-8 + 1 - decoder_op))
                with tf.variable_scope("latent"):
                    latent_loss_op = -0.5 * tf.reduce_sum(
                        1 + tf.log(tf.square(sampler_logdev_op))
                        - tf.square(sampler_mean_op)
                        - tf.square(sampler_logdev_op), 1)
                loss_op = tf.reduce_mean(decoder_loss_op + latent_loss_op)
            train_summaries.append(tf.summary.scalar('decoder_loss', tf.reduce_mean(decoder_loss_op)))
            train_summaries.append(tf.summary.scalar("latent_loss", tf.reduce_mean(latent_loss_op)))
            train_summaries.append(tf.summary.scalar("loss", loss_op))

            with tf.variable_scope("optimizer"):
                if TrainConfig.LEARNING_RATE_DECAY:
                    learning_rate = tf.train.exponential_decay(
                        TrainConfig.LEARNING_RATE, step_op, 100, TrainConfig.LEARNING_RATE_DECAY)
                    train_summaries.append(tf.summary.scalar('learning_rate', learning_rate))
                    # trainer = tf.train.MomentumOptimizer(learning_rate, 0.5)
                    trainer = tf.train.AdamOptimizer(learning_rate)
                else:
                    trainer = tf.train.AdamOptimizer(TrainConfig.LEARNING_RATE)

                train_op = trainer.minimize(loss_op, global_step=step_op)
        print("Initializing train... Done")

        print("Initializing variables...", end="\r")
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        print("Initializing variables... Done")

        # Training loop

        train_sample_count = len(train_images)
        step_per_epoch = int(train_sample_count / TrainConfig.BATCH_SIZE)
        if (step_per_epoch * TrainConfig.BATCH_SIZE) < train_sample_count:
            step_per_epoch += 1

        print("Initializing saver...", end="\r")
        if args.checkpoint:
            if os.path.exists(args.checkpoint):
                shutil.rmtree(args.checkpoint)
            os.makedirs(args.checkpoint)

            # Saving the configuration as JSON files
            with open(os.path.join(args.checkpoint, "train_config.json"), "w") as config_file:
                config_dict = {}
                for key in TrainConfig.__dict__:
                    if key[0] != "_":
                        config_dict[key] = getattr(TrainConfig, key)
                config_file.write(json.dumps(config_dict, indent=3))
            with open(os.path.join(args.checkpoint, "model_config.json"), "w") as config_file:
                config_dict = {}
                for key in ModelConfig.__dict__:
                    if key[0] != "_":
                        config_dict[key] = getattr(ModelConfig, key)
                config_file.write(json.dumps(config_dict, indent=3))

            summary_op = tf.summary.merge(train_summaries + network_output["summaries"])
            image_op = tf.summary.merge(image_summaries)
            test_image_op = tf.summary.merge(test_summaries)
            train_writer = tf.summary.FileWriter(os.path.join(args.checkpoint, "train"), sess.graph, flush_secs=30)
            # tf.all_variables() tf.global_variables(scope="Network") or tf.trainable_variables(scope="Network")
            saver = tf.train.Saver(tf.global_variables(scope="Network"), max_to_keep=TrainConfig.MAX_CHECKPOINT)
        else:
            train_writer = None
        print("Initializing saver... Done")

        def save_summary(batch_norm_train):
            if train_writer:
                summary = sess.run(
                    summary_op,
                    feed_dict={
                        symbolic_input: batch_images,
                        is_training: True,
                        test_latent: dummy_latent,
                        batch_norm_training: batch_norm_train})
                train_writer.add_summary(summary, global_step=global_step)
            saver.save(sess, os.path.join(args.checkpoint, "checkpoint"), global_step=global_step)

        print("\nTraining... (%d samples, %d steps per epoch)" % (train_sample_count, step_per_epoch))

        train_start_time = time.time()
        benchmark_time = time.time()
        index_list = np.arange(train_sample_count)

        epoch = 0
        batch_iterator = 0
        speed = 0

        # Latent values for test image
        test_batch_size = TrainConfig.TEST_IMAGE_ROWS * TrainConfig.TEST_IMAGE_COLUMNS
        dummy_images = np.zeros(
            (test_batch_size,
             TrainConfig.OUTPUT_SHAPE[0] * TrainConfig.OUTPUT_SHAPE[1] * TrainConfig.OUTPUT_SHAPE[2]))
        x_values = np.linspace(-3, 3, TrainConfig.TEST_IMAGE_ROWS)
        y_values = np.linspace(-3, 3, TrainConfig.TEST_IMAGE_COLUMNS)
        test_latent_values = np.asarray([np.meshgrid(x_values, y_values)]).reshape(
            ModelConfig.LATENT_DIMENSIONS, -1).T

        batch_images = []
        dummy_latent = np.zeros([TrainConfig.BATCH_SIZE, ModelConfig.LATENT_DIMENSIONS])
        try:
            # from tensorflow.python.client import timeline
            global_step = 0
            batch_norm_train = True
            saver.save(sess, os.path.join(args.checkpoint, "checkpoint_0"))

            for epoch in range(TrainConfig.MAX_EPOCH):
                print("\nEpoch %d" % epoch)
                np.random.shuffle(index_list)
                batch_iterator = 0
                benchmark_time = time.time()
                # if TrainConfig.BATCH_NORM_TRAIN_START and epoch > TrainConfig.BATCH_NORM_TRAIN_START:
                #     batch_norm_train = not batch_norm_train
                # if global_step % 10 == 0:
                for batch_iterator in range(step_per_epoch):
                    if global_step % 20 == 0:
                        if batch_iterator:
                            speed = batch_iterator / (time.time() - benchmark_time)
                        print("Step %d, %0.02f steps/s, %0.02f input/sec" % (
                            global_step, speed, speed * TrainConfig.BATCH_SIZE), end="\r")

                    batch_images = []
                    if (batch_iterator + 1) * TrainConfig.BATCH_SIZE < train_sample_count:
                        batch_images = train_images[
                            index_list[
                                batch_iterator * TrainConfig.BATCH_SIZE:
                                    (batch_iterator + 1) * TrainConfig.BATCH_SIZE]]
                    else:
                        batch_images = train_images[
                            index_list[batch_iterator * TrainConfig.BATCH_SIZE:train_sample_count]]

                    if global_step % int(TrainConfig.STEPS_BEFORE_IMAGES) == 0:
                        _, images = sess.run(
                            [train_op, image_op],
                            feed_dict={
                                symbolic_input: batch_images,
                                is_training: True,
                                test_latent: dummy_latent,
                                batch_norm_training: batch_norm_train})
                        train_writer.add_summary(images, global_step=global_step)
                        # train_writer.flush()
                    else:
                        sess.run(
                            train_op,
                            feed_dict={
                                symbolic_input: batch_images,
                                is_training: True,
                                test_latent: dummy_latent,
                                batch_norm_training: batch_norm_train})

                    if global_step % int(TrainConfig.STEPS_BEFORE_SUMMARIES) == 0:
                        save_summary(batch_norm_train)
                        # test_accuracy()
                    if global_step % int(TrainConfig.STEPS_BEFORE_TEST) == 0:
                        test_images = sess.run(decoder_op, feed_dict={
                            symbolic_input: dummy_images,
                            test_latent: test_latent_values,
                            is_training: False,
                            batch_norm_training: batch_norm_train})

                        final_image = np.empty([
                            TrainConfig.OUTPUT_SHAPE[0] * TrainConfig.TEST_IMAGE_COLUMNS,
                            TrainConfig.OUTPUT_SHAPE[1] * TrainConfig.TEST_IMAGE_ROWS,
                            TrainConfig.OUTPUT_SHAPE[2]])
                        image_iterator = 0
                        for i in range(TrainConfig.TEST_IMAGE_COLUMNS):
                            for j in range(TrainConfig.TEST_IMAGE_ROWS):
                                final_image[
                                        i*TrainConfig.OUTPUT_SHAPE[1]: (i+1)*TrainConfig.OUTPUT_SHAPE[1],
                                        j*TrainConfig.OUTPUT_SHAPE[0]: (j+1)*TrainConfig.OUTPUT_SHAPE[0]
                                    ] = test_images[image_iterator]
                                image_iterator += 1

                        test_image_result = sess.run(test_image_op, feed_dict={test_image: [final_image]})
                        train_writer.add_summary(test_image_result, global_step=global_step)
                    global_step += 1
        except KeyboardInterrupt:
            pass
        save_summary(batch_norm_train)

        train_stop_time = time.time()

        train_memory_peak = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
        train_gpu_memory = subprocess.check_output(
            "nvidia-smi --query-gpu=memory.used --format=csv,noheader", shell=True).decode()

        if "CUDA_VISIBLE_DEVICES" in os.environ:
            train_gpu_memory = train_gpu_memory.split("\n")[int(os.environ["CUDA_VISIBLE_DEVICES"])]
        else:
            train_gpu_memory = " ".join(train_gpu_memory.split("\n"))

        result = "Training time : %gs\n\tRAM peak : %g MB\n\tVRAM usage : %s" % (
            train_stop_time - train_start_time,
            int(train_memory_peak / 1024),
            train_gpu_memory)
        print(result)
        # with open("output.txt", "w") as output_file:
        #     output_file.write(result)


if __name__ == '__main__':
    train()
